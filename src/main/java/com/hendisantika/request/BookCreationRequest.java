package com.hendisantika.request;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-mysql
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/13/22
 * Time: 21:51
 * To change this template use File | Settings | File Templates.
 */
@Data
public class BookCreationRequest {
    private String name;
    private String isbn;
    private Long authorId;
}
