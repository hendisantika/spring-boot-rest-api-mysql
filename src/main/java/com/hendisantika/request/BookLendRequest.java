package com.hendisantika.request;

import lombok.Data;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-mysql
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/13/22
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */
@Data
public class BookLendRequest {
    private List<Long> bookIds;
    private Long memberId;
}
