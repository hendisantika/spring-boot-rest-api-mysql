package com.hendisantika.repository;

import com.hendisantika.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-mysql
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/13/22
 * Time: 21:54
 * To change this template use File | Settings | File Templates.
 */
public interface MemberRepository extends JpaRepository<Member, Long> {
}
