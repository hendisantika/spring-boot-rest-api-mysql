package com.hendisantika.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-mysql
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/13/22
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
public enum LendStatus {
    AVAILABLE, BURROWED
}
