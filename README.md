# Spring Boot REST API Using JPA, Hibernate, MySQL Tutorial

<p align="left">
In this article, I’m going to explain how we can build a REST API using JPA, Hibernate, and MySQL in Spring Boot.

Here JPA (Java Persistence API) used to access, manage, and persist data between java and relational database while
hibernate is doing the ORM(Object Relational Mapping) part.

Technologies Going to Use,

- Java 17
- Spring Boot: 2.7.3
- JPA
- MySQL
- Lombok
- Maven
- IntelliJ Idea Ultimate

Main topics I’m going to discuss here,

- Adding Required Dependencies
- Defining API Endpoints
- Developing the API
    - Configure MySQL for Spring Boot Application
        - spring.jpa.hibernate.ddl-auto explanation
    - Writing Model Classes
        - Defining Relationships Between Model Classes
        - JPA Cascade Types
        - Using Enum in JPA Model Classes
        - What are JsonBackReference and JsonManagedReference
    - Defining Repository Layer
    - Adding Service Layer
    - Controller Layer to Expose REST API Endpoints
- Testing API
- Further Development
- Moving from MySQL to PostgreSQL in Spring Boot
- Conclusion

</p>

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-rest-api-mysql.git`
2. Navigate to the folder: `cd spring-boot-rest-api-mysql`
3. Set you own Database Credentials
4. Run the application: `mvn clean spring boot:run`
5. Open POSTMAN Collection

#### Create Author

Response

```shell
curl --location --request POST 'http://localhost:8080/api/library/author' \
--header 'Content-Type: application/json' \
--data-raw '{
	"firstName": "John",
	"lastName": "Doe"
}'
```

Response

```shell
{
    "id": 1,
    "name": "The Body: A Guide for Occupants",
    "isbn": "0316535575",
    "author": {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe"
    }
}
```

#### Create Book

Request:

```shell
curl --location --request POST 'http://localhost:8080/api/library/book' \
--header 'Content-Type: application/json' \
--data-raw '{
	"name": "The Body: A Guide for Occupants",
	"isbn": "0316535575",
	"authorId": 1
}'
```

Response

```shell
{
    "id": 1,
    "name": "The Body: A Guide for Occupants",
    "isbn": "0316535575",
    "author": {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe"
    }
}
```

### Create Member

Request

```shell
curl --location --request POST 'http://localhost:8080/api/library/member' \
--header 'Content-Type: application/json' \
--data-raw '{
	"firstName": "Johannes",
	"lastName": "Peters"
}'
```

Response:

```shell
{
    "id": 1,
    "firstName": "Johannes",
    "lastName": "Peters",
    "status": "ACTIVE"
}
```

### Read Books

Request

```shell
curl --location --request GET 'http://localhost:8080/api/library/book'
```

Response

```shell
[
    {
        "id": 1,
        "name": "The Body: A Guide for Occupants",
        "isbn": "0316535575",
        "author": {
            "id": 1,
            "firstName": "John",
            "lastName": "Doe"
        }
    }
]
```

### Read Book By ISBN

Request

```shell
curl --location --request GET 'http://localhost:8080/api/library/book?isbn=0316535575'
```

Response:

```shell
{
    "id": 1,
    "name": "The Body: A Guide for Occupants",
    "isbn": "0316535575",
    "author": {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe"
    }
}
```

### Read Book By ID

Request

```shell
curl --location --request GET 'http://localhost:8080/api/library/book/1'
```

Response

```shell
{
    "id": 1,
    "name": "The Body: A Guide for Occupants",
    "isbn": "0316535575",
    "author": {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe"
    }
}
```

### Lend A Book

Request

```shell
curl --location --request POST 'http://localhost:8080/api/library/book/lend' \
--header 'Content-Type: application/json' \
--data-raw '{
    "bookIds": [
        1
    ],
    "memberId": 1
}'
```

Response

```shell
[
    "The Body: A Guide for Occupants"
]
```